// Create fruits document
db.fruits.insertMany([
        {
         "name": "Apple",   
         "color": "Red",
         "stock": 20,
         "price": 40,
         "supplier_id": 1,
         "onSale": true,
         "origin": ["Philippines", "US"]
        },
        {
         "name": "Banana",   
         "color": "Yellow",
         "stock": 15,
         "price": 20,
         "supplier_id": 2,
         "onSale": true,
         "origin": ["Philippines", "Ecuador"]
        },
        {
         "name": "Kiwi",   
         "color": "Green",
         "stock": 25,
         "price": 50,
         "supplier_id": 1,
         "onSale": true,
         "origin": ["China", "US"]
        },
        {
         "name": "Mango",   
         "color": "Yellow",
         "stock": 10,
         "price": 120,
         "supplier_id": 2,
         "onSale": false,
         "origin": ["Philippines", "India"]
        }
]);
/*MongoDB Aggregation
	-used to generate manipulated data and perform operations to create filteres results that helps in analyzing data.
*/

// Aggregate Method

/*$match
	-used to pass documents that meet the specified condition(s) to the next pipeline stage/aggregation process.

	Syntax:
	{$match: {field: value}}
*/

/*$group
	-used to group elements together and field-value pairs using the data from the grouped elements.

	Syntax:
	{$group: {_id: "value", fieldResult: "valueResult"}}
*/
db.fruits.aggregate([
	{$match: {"onSale": true}},
	{$group: {"_id": "$supplier_id", "total":{$sum: "$stock"}}}
]);

// Sorting Aggregated Results

/*$sort
	-can be used to change the order of aggregated result.

	Syntax:
	{$sort: {field: 1/-1}}
	1 - ascending order
	-1 - descending order
*/
db.fruits.aggregate([
	{$match: {"onSale": true}},
	{$group: {"_id": "$supplier_id", "total":{$sum: "$stock"}}},
	{$sort: {"total": -1}}
]);

// Aggregating results based on array fields
/*$unwind
	-deconstruct an array field from a collection/field with an array value to output a result for each element.

	Syntax:
	{$unwind: field} 
*/
db.fruits.aggregate([
	{$unwind: "$origin"},
	{$group: {_id: "$origin", kinds: {$sum: 1}}}
]);

// Schema Design

// One-to-One Relationship

var owner = ObjectId();

db.owners.insert({
	_id: owner,
	name : "John Smith",
	contact: "09151234567"
});

db.suppliers.insert({
	name: "ABC Fruits",
	contact: "09191234567",
	owner_id: ObjectId("6177704a28f34e2310155222")
});

db.owners.updateOne(
	{"_id": ObjectId("6177704a28f34e2310155222")},	
	{$set: {"supplier_id": ObjectId("6177716b28f34e2310155223")}}
);

// One-to-Many Relationship
db.suppliers.insertMany([
	{
		"name": "DEF Fruits",
		"contact": "09151234567",
		"owner_id": ObjectId("6177704a28f34e2310155222")
	},	
	{
		"name": "GHI Fruits",
		"contact": "09181234567",
		"owner_id": ObjectId("6177704a28f34e2310155222")
	}	

]);

db.owners.updateOne(
	{"_id": ObjectId("6177704a28f34e2310155222")},	
	{$set: {"supplier_id": 
                [ObjectId("617779f928f34e2310155224"), 
                ObjectId("617779f928f34e2310155225")]
               }
           }
);

















































































































